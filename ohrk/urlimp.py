from urllib.parse import urlparse, urljoin, quote, unquote, urlencode, parse_qs
from urllib.request import urlopen, urlretrieve
from urllib.error import HTTPError
