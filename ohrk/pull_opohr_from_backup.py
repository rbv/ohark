#!/usr/bin/env python3
"""
Pull game listings from the backup files of the Op:OHR website.
This is more reliable and easier than going through the website,
and also reveals extra screenshots!
You can download the op:ohr backup at
http://tmc.castleparadox.com/ohr/archive/operation_ohr_backup.tar.xz (158MB)
"""

import os
from collections import defaultdict
import shutil

if __name__ == '__main__':
    import ohrkpaths  # Setup sys.path

from ohrk import gamedb, scrape, urlimp, util


# Point this to where you've extracted the above archive
OPOHR_PATH = '../operationohr/'

assert os.path.isfile(OPOHR_PATH + 'gamelist-display.php')

OPOHR_URL = 'http://www.castleparadox.com/archive/operationohr/'

encoding = 'latin-1'

# See docs/tagging.txt about tag remapping.
statuses = {"Finished game": "complete", "Demo released": "demo", "No demo": "no demo"}

unique_extns = '.eml', '.pas', '.dsc', '.aut', '.sta', '.url', '.zip'
expected_extns = '.eml', '.pas', '.dsc', '.aut', '.sta', '.url', '.zip', '.jpg', '.gif', '.LOG', '.cnf'
image_extns = '.gif', '.jpg'

stats = {'extradownloads': 0, 'extragames': 0, 'extrascreenshots': 0}

def process_game(dirname, path):
    # Note: Since we're not reading the files over the web, dirname has one
    # less layer of escaping than the URLs

    # Special cases
    if dirname == 'Skias%20Saga':
        # Skip this game, the files in this directory are all named Last%20Legacy
        # and I don't know which is the right name. (There is a real Last Legacy entry)
        # The info about the game is trivial anyway.
        print("!! Skipping " + dirname)
        return
    if dirname == 'Wally%27s%20Castle':
        # The contents of this directory have a different name, Wally%5C%27s%20Castle,
        # which causes Op:OHR to screw up. The files are identical to the Wally's Castle entry anyway.
        print("!! Skipping " + dirname)
        return
    if dirname in ('Joe%27s%20Great%20Adventure%3A%20The%20Goblet%20of%20Life', 'Last%20Resort%20'):
        # These are duplicate near-blank entries, and have the same name, but with different
        # escaping, and cause collisions if not omitted
        print("!! skipping " + dirname)
        return

    files = os.listdir(path)
    if len(files) == 0:
        print("!! Skipping empty directory " + dirname)
        return

    game = gamedb.Game()
    game.name = util.unescape_filename(dirname)
    srcid = util.id_from_filename(dirname)

    # See what files we've got, whether there are any unexpected ones
    by_extn = defaultdict(list)
    for fname in files:
        if fname in ('_vti_cnf',) or fname.endswith('.LOG'):
            # Ignore this crud
            continue
        extn = os.path.splitext(fname)[1]
        if fname.lower() != dirname.lower() + extn:
            if extn in image_extns:  # We make use of extra screenshots, so don't report
                game.extra_info += "(Note: the page for this game on Op:OHR has a missing screenshot.)\n"
                print(" Note: found game with extra screenshot, %s/%s" % (game.name, fname))
                stats['extrascreenshots'] += 1
            else:
                print(" %s: Extra File %s" % (game.name, fname))
            if extn in unique_extns:
                continue
        elif fname != dirname + extn:
            # This download or screenshot will not work when viewed on Op:OHR because the
            # case does not match
            print(" Note: found game with broken download link or screenshot, " + fname)
            if extn == '.zip':
                stats['extradownloads'] += 1
                game.extra_info += "(Note: the page for this game on Op:OHR has a broken download.)\n"
            elif extn in image_extns:
                stats['extrascreenshots'] += 1
                game.extra_info += "(Note: the page for this game on Op:OHR has a missing screenshot.)\n"

        assert extn in expected_extns
        by_extn[extn].append(fname)


    def getdata(extn):
        assert len(by_extn[extn]) == 1
        fname = by_extn[extn][0]
        return util.read_text_file(os.path.join(path, fname), encoding)

    # Note that we quote it once, and the browser will quote it a second time, as required.
    # Note: there's a game that doesn't show up on the gamelist, '?', but does have a page
    game.url = OPOHR_URL + 'gamelist-display.php?username=' + urlimp.quote(dirname)

    game.author = util.fix_escapes(getdata('.aut'))
    if not game.author:
        print(" %s: Invalid author '%s'" % (game.name, game.author))
    if getdata('.eml') not in ("none", "None", ""):
        game.author_link = "mailto:" + getdata('.eml')
        if '@' not in game.author_link:
            print(" %s: Invalid email '%s'" % (game.name, game.author_link))
    game.description = util.text2html(util.fix_escapes(getdata('.dsc')))
    website = getdata('.url')
    if website:
        if len(website) > 7:
            game.website = website
        elif website != 'None' and website != 'http://':
            print(" Invalid website '%s'" % website)
    status = getdata('.sta')
    if '.zip' in by_extn:
        assert len(by_extn['.zip']) == 1
        fname = by_extn['.zip'][0]
        zip_fname = util.unescape_filename(fname)
        # Note that dirname and filename are quoted twice in download_url
        download_url = OPOHR_URL + urlimp.quote('gamelist/%s/%s' % (dirname, by_extn['.zip'][0]))
        game.downloads = [gamedb.DownloadLink('opohr', util.id_from_filename(fname), download_url, zip_fname)]
        if status == "No demo":
            print(" %s: Status '%s' but game has a download" % (game.name, status))
            game.extra_info += "(Note: the page for this game on Op:OHR has a missing download link.)\n"
            stats['extradownloads'] += 1
            status = None

    if status in statuses:
        game.tags.append(statuses[status])
    elif status:
        print(" Invalid status '%s'" % status)

    for screenshot in by_extn['.jpg'] + by_extn['.gif']:
        # Copy each screenshot to this game's data directory, and register it
        datadir = game.create_datadir(db.name, srcid)
        filename = os.path.join(datadir, screenshot)
        screenshot_url = OPOHR_URL + 'gamelist/' + urlimp.quote(dirname + '/' + screenshot)
        shutil.copy2(os.path.join(path, screenshot), filename)
        game.screenshots.append(gamedb.Screenshot(screenshot_url, filename))

    game = scrape.clean_strings(game)

    assert srcid not in db.games
    db.games[srcid] = game

def process_index(path):
    for dirname in sorted(os.listdir(path)):
        process_game(dirname, os.path.join(path, dirname))


db = gamedb.GameList('opohr')

process_index(os.path.join(OPOHR_PATH, 'gamelist'))

db.save()

print(stats)
