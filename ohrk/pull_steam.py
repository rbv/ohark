

games = [
    "https://store.steampowered.com/app/1193300/Axe_Cop/",
    "https://store.steampowered.com/app/1761680/Red_Triangle_Super_Collection/"  # unreleased
    "https://store.steampowered.com/app/574690/Mr_Triangles_Adventure/",
    "https://store.steampowered.com/app/584860/Surfasaurus/",
    "https://store.steampowered.com/app/384950/Macabre/",
    "https://store.steampowered.com/app/385030/C_Kane/",
    "https://store.steampowered.com/app/611710/Kaiju_Big_Battel_Fighto_Fantasy/",
    "https://store.steampowered.com/app/563430/Void_Pyramid/",
    "https://store.steampowered.com/app/1131750/Alien_Squatter/",
    "https://store.steampowered.com/app/1586270/Katjas_Abyss_Tactics/"
]
greenlight = [
    "https://steamcommunity.com/sharedfiles/filedetails/?id=864391600",  # Mr. Triangle's Maze
    "https://steamcommunity.com/workshop/filedetails/93030498",  # Motrya
    "https://steamcommunity.com/sharedfiles/filedetails/?id=411084366",  # Tim-Tim 2: "The Almighty Gnome"
]
#    "https://steamcommunity.com/sharedfiles/filedetails/?id=144319539",  # Bob the Hamster - Dirt Dig  (Not OHR)
