#!/usr/bin/env python3

# NOTE: if the scrapers are run as python 3 and the server as python 2,
# then Game can't be unpickled because __new__ is missing. All other combos work.

import sys
import os
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/..'))

from wsgiref.simple_server import make_server

import ohrk.website

httpd = make_server('', 8080, ohrk.website.application)
print("Serving HTTP on port 8080...")

# Respond to requests until process is killed
httpd.serve_forever()
